(ns app.views
  (:require [app.state :refer [app-state]]
            [app.events :refer [increment decrement]]
            ["rmwc/Button" :refer (Button ButtonIcon)]))

(defn header
  []
  [:div
   [:h1 "A template for reagent apps"]])

(defn counter
  []
  [:div
   [:> Button {:raised true :on-click #(decrement %)} [:> ButtonIcon {:strategy "className" :basename "zmdi" :prefix "zmdi-" :use "minus"}]]
   [:> Button {:raised true :disabled true} (get @app-state :count)]
   [:> Button {:raised true :on-click #(increment %)} [:> ButtonIcon {:strategy "className" :basename "zmdi" :prefix "zmdi-" :use "plus"}]]])

(defn app []
  [:div
   [header]
   [counter]])
